export const USER_LOGIN = "USER_LOGIN";
export const LocalstorageService = {
  set: (data) => {
    let dataJSON = JSON.stringify(data);
    localStorage.setItem(USER_LOGIN, dataJSON);
  },
  get: () => {
    let dataJSON = localStorage.getItem(USER_LOGIN);
    if (dataJSON) {
      return JSON.parse(dataJSON);
    }
    {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(USER_LOGIN);
  },
};
