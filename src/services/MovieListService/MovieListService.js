import axios from "axios";
import { createConfig } from "../ConfigURL/ConfigURL";
import { BASE_URL } from "../constant/constant";

export const movieListService = {
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getThongTinPhim: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getPhimTheoHeThongRap: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
