import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieListService } from "../../services/MovieListService/MovieListService";
import { Tabs } from "antd";
import YoutubeEmbed from "../../Components/YoutubeEmbed/YoutubeEmbed";
import moment from "moment";
export default function MovieDetailPage() {
  let param = useParams();
  let movieId = param.id;
  const [movieDetail, setMovieDetail] = useState({});
  useEffect(() => {
    movieListService
      .getThongTinPhim(movieId)
      .then((res) => {
        setMovieDetail(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);
  const renderMovieDetail = () => {
    let { tenPhim, trailer, hinhAnh, danhGia, moTa, ngayKhoiChieu, biDanh } =
      movieDetail;
    return (
      <div className="grid grid-cols-12 gap-9 mx-auto p-14">
        <div className="hero-banner col-span-5 flex justify-center">
          <img
            style={{ width: "430px", height: "550px", objectFit: "cover" }}
            src={hinhAnh}
            alt={biDanh}
          />
        </div>
        <div className="title col-span-7">
          {/* <div className="grid grid-cols-12 pb-3"> */}
          <div className="md:flex justify-between items-center mb-3">
            <h2 className="col-span-10 font-semibold sm:mb-5">{tenPhim}</h2>
            <span className="text-2xl col-span-2 font-semibold px-3  py-1 rounded bg-yellow-400">
              {danhGia}
              <i className="fa fa-star ml-2"></i>
            </span>
          </div>
          <div>
            Ngày khởi chiếu:{" "}
            <span className="bg-red-500 px-3 py-1 rounded text-white ml-2">
              {moment(ngayKhoiChieu).format("DD/MM/YYYY - hh:mm")}
            </span>
          </div>
          <div className="pt-5">
            <Tabs
              defaultActiveKey="1"
              items={[
                {
                  label: `OVERVIEW`,
                  key: "1",
                  children: `${moTa}`,
                },
                {
                  label: `TRAILER`,
                  key: "2",
                  children: <YoutubeEmbed embedId={trailer} />,
                  // <iframe
                  //   src={trailer}
                  //   frameborder="0"
                  //   allow="autoplay; encrypted-media"
                  //   allowfullscreen
                  //   title="video"
                  // />
                },
              ]}
            />
          </div>
        </div>
      </div>
    );
  };
  return <div>{renderMovieDetail()}</div>;
}
