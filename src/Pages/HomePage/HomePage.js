import React, { useEffect, useState } from "react";
import Banner from "../../Components/Banner/Banner";
import DownloadApp from "../../Components/DownloadApp/DownloadApp";
import { movieListService } from "../../services/MovieListService/MovieListService";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  const [movieArray, setMovieArray] = useState([]);
  useEffect(() => {
    movieListService
      .getDanhSachPhim()
      .then((res) => {
        setMovieArray(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);

  return (
    <div className="mx-auto">
      <Banner />
      <MovieList movieArray={movieArray} />
      <MovieTabs />
      <DownloadApp />
    </div>
  );
}
