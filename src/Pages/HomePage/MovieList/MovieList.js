import React from "react";
import { Button, Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function MovieList({ movieArray }) {
  const renderMovieList = () => {
    return movieArray.slice(0, 16).map((item) => {
      let { maPhim, tenPhim, hinhAnh } = item;
      return (
        <Card
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img alt="example" src={hinhAnh} className="h-96 object-cover" />
          }
          actions={[
            <Button className="bg-red-500">
              <NavLink style={{ color: "white" }} to={`/detail/${maPhim}`}>
                Xem chi tiết
              </NavLink>
            </Button>,
          ]}
        >
          <Meta title={tenPhim} />
        </Card>
      );
    });
  };
  return (
    <div className=" py-10">
      <div className="flex justify-center pb-10 font-semibold">
        <h2>Danh sách phim HOT đầu năm 2023</h2>
      </div>
      <div className="grid sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-10 place-items-center container mx-auto">
        {renderMovieList()}
      </div>
    </div>
  );
}
