import React from "react";
import moment from "moment";
export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3">
      <img
        className="w-24 h-40 object-cover mr-5 rounded"
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        {/* moment js */}
        <h5 className="font-medium mb-5">{movie.tenPhim}</h5>
        <div className="lg:grid lg:grid-cols-3 lg:gap-4 sm:flex sm:flex-col sm:gap-5">
          {movie.lstLichChieuTheoPhim.slice(0, 9).map((item) => {
            return (
              <span className="bg-red-500 text-white rounded p-2">
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
