import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
import { movieListService } from "../../../services/MovieListService/MovieListService";
const onChange = (key) => {
  console.log(key);
};
export default function MovieTabs() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    movieListService
      .getPhimTheoHeThongRap()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: (
          <img
            className="w-16 h-16 object-cover"
            src={heThongRap.logo}
            alt=""
          />
        ),
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                label: (
                  <div>
                    <p>{cumRap.tenCumRap}</p>
                  </div>
                ),
                key: cumRap.maCumRap,
                children: cumRap.danhSachPhim.map((phim) => {
                  return <MovieTabItem movie={phim} />;
                }),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <div className="py-10 container">
      <div className="flex justify-center pb-10 font-semibold">
        <h2>Hệ thống rạp chiếu rộng khắp Việt Nam</h2>
      </div>
      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
