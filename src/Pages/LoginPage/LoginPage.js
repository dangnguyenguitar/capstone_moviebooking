import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { UserService } from "../../services/UserService/UserService";
import { useDispatch } from "react-redux";
import { setUserInfor } from "../../redux/slice/userSlice";
import { LocalstorageService } from "../../services/LocalstorageService/LocalstorageService";

export default function LoginPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const onFinish = (values) => {
    UserService.postDangNhap(values)
      .then((res) => {
        console.log(res);
        dispatch(setUserInfor(res.data.content));
        message.success("Đăng nhập thành công! Tự động chuyển hướng sau 2s");
        LocalstorageService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã có lỗi xảy ra! Đăng nhập không thành công.");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      style={{
        backgroundImage: "url('./assets/images/background-download.jpg')",
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
      className="h-screen flex items-center justify-center"
    >
      <div className=" bg-white rounded-2xl p-10">
        <div className="text-center font-semibold mb-5">
          <i className="fa-solid fa-user text-3xl text-orange-600 mb-3" />
          <h3>ĐĂNG NHẬP</h3>
        </div>
        <div>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Checkbox>Nhớ tài khoản</Checkbox>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button
                className="rounded bg-orange-600 text-white hover:bg-transparent w-full"
                htmlType="submit"
              >
                Đăng nhập
              </Button>
              <small>
                Bạn chưa có tài khoản?{" "}
                <NavLink to="/register">
                  <span className="underline">ĐĂNG KÝ NGAY</span>
                </NavLink>
              </small>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
