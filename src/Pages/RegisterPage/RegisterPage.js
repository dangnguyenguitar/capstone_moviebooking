import React from "react";
import { Button, Form, Input, message, Space } from "antd";
import { UserService } from "../../services/UserService/UserService";
const onFinish = (values) => {
  console.log("Success:", values);
  UserService.postDangKy(values)
    .then((res) => {
      console.log(res);
      message.success("Đăng ký tài khoản thành công!");
    })
    .catch((err) => {
      console.log(err);
      message.error("Đã xảy ra lỗi trong quá trình đăng ký");
    });
};
const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};

export default function RegisterPage() {
  return (
    <div
      style={{
        backgroundImage: "url('./assets/images/background-download.jpg')",
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
      className=" h-screen flex items-center justify-center"
    >
      <div className=" bg-white rounded-2xl p-10">
        <div className="text-center font-semibold mb-5">
          <i className="fa-solid fa-user text-3xl text-orange-600 mb-3" />
          <h3>ĐĂNG KÝ TÀI KHOẢN</h3>
        </div>
        <div>
          <Form
            name="basic"
            labelCol={{
              span: 10,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Xin hãy nhập tên tài khoản",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Xin hãy nhập mật khẩu",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="Nhập lại mật khẩu"
              name="nhapLaiMatKhau"
              rules={[
                {
                  required: true,
                  message: "Xin hãy nhập lại mật khẩu một lần nữa",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="Họ tên"
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Xin hãy nhập đầy đủ họ tên!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="SĐT"
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Xin hãy nhập số điện thoại!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Xin hãy nhập Email!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 10,
                span: 24,
              }}
            >
              <Space
                direction="vertical"
                style={{
                  width: "100%",
                }}
              >
                <Button
                  className="rounded bg-orange-600 text-white hover:bg-transparent"
                  htmlType="submit"
                  block
                >
                  Đăng ký
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
