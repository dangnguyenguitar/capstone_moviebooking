import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { LocalstorageService } from "../../services/LocalstorageService/LocalstorageService";
export default function UserReg() {
  let user = useSelector((state) => {
    return state.userSlice.userInfor;
  });
  const handleDangXuat = () => {
    LocalstorageService.remove();
    window.location.href = "/";
  };
  const renderContent = () => {
    if (user) {
      let { hoTen } = user;
      return (
        <div>
          <span className="px-3 py-2 border-r-2">
            Welcome back!{" "}
            <span className="text-red-500 font-medium ">{hoTen}</span>
          </span>
          <button
            onClick={handleDangXuat}
            className="px-3 py-2 border border-neutral-500 rounded ml-3 hover:text-red-500 hover:border-red-500"
          >
            Đăng xuất
          </button>
        </div>
      );
    }
    {
      return (
        <div>
          <NavLink to="/login">
            <button className="px-3 py-2 border rounded mr-3 hover:text-black hover:border-red-500 bg-red-500 text-white">
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to="/register">
            <button className="px-3 py-2 border border-neutral-500 rounded mr-3 hover:text-red-500 hover:border-red-500">
              Đăng ký
            </button>
          </NavLink>
        </div>
      );
    }
  };
  return <div>{renderContent()}</div>;
}
