import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Hamburger from "./Hamburger";
import UserReg from "./UserReg";
import "./header.css";
export default function Header() {
  let logo = "/assets/images/movie-logo-red.png";
  const [showNavbar, setShowNavbar] = useState(false);
  const handleToggleNavbar = () => {
    setShowNavbar(!showNavbar);
  };
  return (
    <div className="grid grid-cols-12 justify-between items-center mx-auto container">
      <div className="col-span-4">
        <NavLink to="/">
          <img className="h-20 w-20 object-contain" src={logo} alt="" />
        </NavLink>
      </div>
      <div
        className={`nav-elements ${
          showNavbar && `active`
        } col-span-8 flex justify-between`}
      >
        <ul className="flex lg:space-x-10 font-semibold lg:items-center">
          <li className="hover:text-red-600 transition duration-150 ">
            <a href="#">Lịch chiếu</a>
          </li>
          <li className="hover:text-red-600 transition duration-150">
            <a href="#">Cụm rạp</a>
          </li>
          <li className="hover:text-red-600 transition duration-150">
            <a href="#">Tin tức</a>
          </li>
          <li className="hover:text-red-600 transition duration-150">
            <a href="#">Ứng dụng</a>
          </li>
        </ul>
        <div className="nav-cta flex justify-center sm:pt-10 lg:pt-0">
          <UserReg />
        </div>
      </div>
      <div className="col-span-8 flex justify-end">
        <button className="menu-icon " onClick={handleToggleNavbar}>
          <Hamburger />
        </button>
      </div>
    </div>
  );
}
