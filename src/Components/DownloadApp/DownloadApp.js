import React from "react";

export default function DownloadApp() {
  return (
    <div
      style={{
        backgroundImage: "url('./assets/images/background-download.jpg')",
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
      className="flex justify-center items-center p-10 gap-x-7 text-white"
    >
      <div className="title w-1/2">
        <h2 className="mb-5 font-semibold">
          Ứng dụng tiện lợi dành cho người yêu điện ảnh
        </h2>
        <p className="mb-5">
          Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi
          quà hấp dẫn.
        </p>
        <button className="py-2 px-4 rounded bg-orange-600 text-white font-semibold hover:bg-orange-800 transition mb-5">
          TẢI NGAY APP MIỄN PHÍ
        </button>
        <p>
          THE FILM ZONE có hai phiên bản{" "}
          <a className="underline" href="#">
            IOS
          </a>{" "}
          &{" "}
          <a className="underline" href="">
            Android
          </a>
        </p>
      </div>
      <div className="application">
        <img
          className="w-full h-96"
          src="./assets/images/banner-slider-3.33a486d1.jpg"
          alt=""
        />
      </div>
    </div>
  );
}
